Release Notes
=============

The following release notes detail the updates to packages available on
http://packages.bitorobotics.com. For more details on the changes that
have occured in an individual package, please see the CHANGELOG within
the installed package.