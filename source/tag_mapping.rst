二维码地图
-----------

建图主要分为四大步骤

A. 配置建图参数
B. 执行建图
C. 保存地图至本地
D. 将地图上传至数据库


A. 配置建图参数
++++++++++++

1. 准备一台用于建图的电脑（已安装Bito建图软件）、一个相机（带特殊镜头且内参已标定）和一根相机连接电脑的线
2. 在home/nami_web/文件夹下打开名为local_config.py的文件

.. figure:: _static/tag_mapping/local_config.png
   :width: 100%
   :align: center
   :figclass: align-centered

3. 在上一步打开的文件中设置WS_HOST为本机IP地址，设置MYSQL_HOST为当前韩信的IP地址，并保存

.. figure:: _static/tag_mapping/WS_host.png
   :width: 100%
   :align: center
   :figclass: align-centered

4. 在terminator里打开 2个终端，分别输入命令：
（1）roslaunch rosbridge_server rosbridge_websocket.launch
（2）python nami_web/start_nami.py

.. figure:: _static/tag_mapping/start_nami.png
   :width: 100%
   :align: center
   :figclass: align-centered

5. 打开浏览器，在地址栏中输入：127.0.0.1:8989

.. figure:: _static/tag_mapping/8989.png
   :width: 100%
   :align: center
   :figclass: align-centered

6. 点击”Set Camera Strem Config”，根据部署现场实际的情况，在弹窗中修改“tag_family_vec” (二维码类型，默认为Tag49h14)、“tag_id_lower_bound_vec” (部署现场用到的最小二维码号码)和”tag_id_upper_bound_vec” (部署现场用到的最大二维码号码)，确认修改后点击“Submit Camera Config”

.. figure:: _static/tag_mapping/camera_config.png
   :width: 100%
   :align: center
   :figclass: align-centered

7. 点击”Set Tag Information Config”，根据部署现场实际的情况，在弹窗中填写二维码号码及对应的尺寸信息，确认后点击“Submit Tag ID Info Config”

.. figure:: _static/tag_mapping/save_image_config.png
   :width: 100%
   :align: center
   :figclass: align-centered

8. 点击”Set Constant Tag”，根据部署现场实际的情况，在弹窗中填写建图初始二维码号码，其他设置为０，确认后点击“Submit Constant Tag”

.. figure:: _static/tag_mapping/constant_tag.png
   :width: 100%
   :align: center
   :figclass: align-centered

9. 点击”Set Tag Mapping Config”，根据现场实际情况，在弹窗中修改”boundingBoxThreshold” (默认值为10)和”angleThreshold” (默认值为45)，确认后点击“Submit”；

.. figure:: _static/tag_mapping/save_tag_mapping.png
   :width: 100%
   :align: center
   :figclass: align-centered


B. 执行建图
+++++++++

10. 连接电脑和相机，相机安装高度约为2m, 角度垂直向下，携带建图设备行走至建图初始二维码号码处
11. 切换到Mapping页面, 点击”Start Mapping”和”Start Camera”开始建图

.. figure:: _static/tag_mapping/start_mapping.png
   :width: 100%
   :align: center
   :figclass: align-centered

12. 按照预定行走路线扫描地图中的二维码，建议行走路线为增量式行走，建图时应尽量保证相机能同时看到已加入地图中的二维码和未加入地图中的二维码

.. figure:: _static/tag_mapping/walk_way.png
   :width: 100%
   :align: center
   :figclass: align-centered

13. 当新加入的二维码在地图中的位置与实际二维码在地图中的位置差距过大时，点击“Undo”撤销上一步加入的二维码

.. figure:: _static/tag_mapping/others.png
   :width: 100%
   :align: center
   :figclass: align-centered

14. 当需要删除某个二维码时，输入二维码号码并点击”Remove Tag”, 会删除此二维码及在此二维码之后新加入地图中的二维码

15. 当发现二维码比较难以加入地图中时，可适当增大角度参数(0~90)并点击”Set Angle”，也可以适当增大绑定框(0~50)并点击”Set Bounding Box”



C. 保存地图至本地
++++++++++++++++++++++++

16. 当得到想要的地图时，点击”Save Map”保存地图至本机

17. 点击“Stop Camera”和”Stop Mapping”关闭相机，结束建图


D. 将地图上传至数据库
++++++++++++++++++++++++++

18. 当需要为地图命名时，输入英文名字并点击“upload”, 将地图上传至数据库；当不需要为地图命名时，直接点击”upload”,　系统将会自动给地图分配一个名字


温馨提示
+++++++++++

1. 建图过程中当地图效果比较好时，记得常点击”Save Map”保存地图

2. 新加入的二维码初始位置不太好时，在此二维码周围多停留，让相机以不同角度看到二维码，待其调整为较好位置时，再继续建图



