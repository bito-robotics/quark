控制API
-------

恢复机器人
~~~~~~~~~~

**简要描述：**

-  恢复机器人

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/controller/resume_robot``

**请求方式：**

-  GET

**请求示例：**

-  ``http://192.168.124.225:7777/api/v1/controller/resume_robot``

**请求参数说明：**

无

**成功返回示例：** json

.. code:: json

    {
      "errmsg": "Resume_OK",
      "errno": "0",
      "serial": "yg00b10018112315000n00"
    }

**返回参数说明：**

+----------+----------+----------------+
| 参数名   | 类型     | 说明           |
+==========+==========+================+
| serial   | string   | 机器人序列号   |
+----------+----------+----------------+
| errno    | string   | API错误码      |
+----------+----------+----------------+
| errmsg   | string   | 错误信息       |
+----------+----------+----------------+

**错误返回示例：** json

.. code:: json

    {
      "errmsg": "Failure to resume robot trajectory motion",
      "errno": "4005",
      "serial": "yg00b10018112315000n00"
      }

**返回参数说明：**

+----------+----------+----------------+
| 参数名   | 类型     | 说明           |
+==========+==========+================+
| serial   | string   | 机器人序列号   |
+----------+----------+----------------+
| errno    | string   | API错误码      |
+----------+----------+----------------+
| errmsg   | string   | 错误信息       |
+----------+----------+----------------+

暂停机器人
~~~~~~~~~~

**简要描述：**

-  暂停机器人

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/controller/pause_robot``

**请求方式：**

-  GET

**请求示例：**

-  ``http://192.168.124.225:7777/api/v1/controller/pause_robot``

**请求参数说明：**

无

**成功返回示例：** json

.. code:: json

    {
      "errmsg": "Pause_OK",
      "errno": "0",
      "serial": "yg00b10018112315000n00"
    }

**返回参数说明：**

+----------+----------+----------------+
| 参数名   | 类型     | 说明           |
+==========+==========+================+
| serial   | string   | 机器人序列号   |
+----------+----------+----------------+
| errno    | string   | API错误码      |
+----------+----------+----------------+
| errmsg   | string   | 错误信息       |
+----------+----------+----------------+

**错误返回示例：** json

.. code:: json

    {
      "errmsg": "Failure to pause robot trajectory motion",
      "errno": "4005",
      "serial": "yg00b10018112315000n00"
      }

**返回参数说明：**

+----------+----------+----------------+
| 参数名   | 类型     | 说明           |
+==========+==========+================+
| serial   | string   | 机器人序列号   |
+----------+----------+----------------+
| errno    | string   | API错误码      |
+----------+----------+----------------+
| errmsg   | string   | 错误信息       |
+----------+----------+----------------+

重置定位
~~~~~~~~

**简要描述：**

-  重置机器人定位

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/controller/reset_localizer``

**请求方式：**

-  GET

**请求示例：**

-  ``http://192.168.124.225:7777/api/v1/controller/reset_localizer``

**请求参数说明：**

无

**成功返回示例：** json

.. code:: json

    {
      "errmsg": "Reset_OK",
      "errno": "0",
      "serial": "yg00b10018112315000n00"
    }

**返回参数说明：**

+----------+----------+----------------+
| 参数名   | 类型     | 说明           |
+==========+==========+================+
| serial   | string   | 机器人序列号   |
+----------+----------+----------------+
| errno    | string   | API错误码      |
+----------+----------+----------------+
| errmsg   | string   | 错误信息       |
+----------+----------+----------------+

**错误返回示例：** json

.. code:: json

    {
      "errmsg": "Reset localizer fail",
      "errno": "4005",
      "serial": "yg00b10018112315000n00"
      }

**返回参数说明：**

+----------+----------+----------------+
| 参数名   | 类型     | 说明           |
+==========+==========+================+
| serial   | string   | 机器人序列号   |
+----------+----------+----------------+
| errno    | string   | API错误码      |
+----------+----------+----------------+
| errmsg   | string   | 错误信息       |
+----------+----------+----------------+

重置轨迹
~~~~~~~~

**简要描述：**

-  重置机器人轨迹

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/controller/reset_navigator``

**请求方式：**

-  GET

**请求示例：**

-  ``http://192.168.124.225:7777/api/v1/controller/reset_navigator``

**请求参数说明：**

无

**成功返回示例：** json

.. code:: json

    {
      "errmsg": "Reset_OK",
      "errno": "0",
      "serial": "yg00b10018112315000n00"
    }

**返回参数说明：**

+----------+----------+----------------+
| 参数名   | 类型     | 说明           |
+==========+==========+================+
| serial   | string   | 机器人序列号   |
+----------+----------+----------------+
| errno    | string   | API错误码      |
+----------+----------+----------------+
| errmsg   | string   | 错误信息       |
+----------+----------+----------------+

**错误返回示例：** json

.. code:: json

    {
      "errmsg": "Reset navigator fail",
      "errno": "4005",
      "serial": "yg00b10018112315000n00"
      }

**返回参数说明：**

+----------+----------+----------------+
| 参数名   | 类型     | 说明           |
+==========+==========+================+
| serial   | string   | 机器人序列号   |
+----------+----------+----------------+
| errno    | string   | API错误码      |
+----------+----------+----------------+
| errmsg   | string   | 错误信息       |
+----------+----------+----------------+

重置伺服
~~~~~~~~

**简要描述：**

-  重置机器人伺服

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/controller/reset_performer``

**请求方式：**

-  GET

**请求示例：**

-  ``http://192.168.124.225:7777/api/v1/controller/reset_performer``

**请求参数说明：**

无

**成功返回示例：** json

.. code:: json

    {
      "errmsg": "Reset_OK",
      "errno": "0",
      "serial": "yg00b10018112315000n00"
    }

**返回参数说明：**

+----------+----------+----------------+
| 参数名   | 类型     | 说明           |
+==========+==========+================+
| serial   | string   | 机器人序列号   |
+----------+----------+----------------+
| errno    | string   | API错误码      |
+----------+----------+----------------+
| errmsg   | string   | 错误信息       |
+----------+----------+----------------+

**错误返回示例：** json

.. code:: json

    {
      "errmsg": "Reset performer fail",
      "errno": "4005",
      "serial": "yg00b10018112315000n00"
      }

**返回参数说明：**

+----------+----------+----------------+
| 参数名   | 类型     | 说明           |
+==========+==========+================+
| serial   | string   | 机器人序列号   |
+----------+----------+----------------+
| errno    | string   | API错误码      |
+----------+----------+----------------+
| errmsg   | string   | 错误信息       |
+----------+----------+----------------+

重置机器人
~~~~~~~~~~

**简要描述：**

-  重置机器人

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/controller/reset_agent_all``

**请求方式：**

-  GET

**请求示例：**

-  ``http://192.168.124.225:7777/api/v1/controller/reset_agent_all``

**请求参数说明：**

无

**成功返回示例：** json

.. code:: json

    {
      "errmsg": "Reset_OK",
      "errno": "0",
      "serial": "yg00b10018112315000n00"
    }

**返回参数说明：**

+----------+----------+----------------+
| 参数名   | 类型     | 说明           |
+==========+==========+================+
| serial   | string   | 机器人序列号   |
+----------+----------+----------------+
| errno    | string   | API错误码      |
+----------+----------+----------------+
| errmsg   | string   | 错误信息       |
+----------+----------+----------------+

**错误返回示例：** json

.. code:: json

    {
      "errmsg": "Reset all fail",
      "errno": "4005",
      "serial": "yg00b10018112315000n00"
      }

**返回参数说明：**

+----------+----------+----------------+
| 参数名   | 类型     | 说明           |
+==========+==========+================+
| serial   | string   | 机器人序列号   |
+----------+----------+----------------+
| errno    | string   | API错误码      |
+----------+----------+----------------+
| errmsg   | string   | 错误信息       |
+----------+----------+----------------+