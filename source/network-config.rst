Network
-------------------

路由器配置
+++++++++

1. 插上电源线开机，电脑通过网线通过lan口与之相连

2. 打开浏览器，输入路由器IP，登陆后设置SSID名称、密码和网段，然后重新启动路由器。

如未更改路由器默认配置，则可以跳过Hanxin和Yugong网络配置。如修改了路由器相关配置，请参照Hanxin和Yugong网络设置，重新配置网络。


传感器网络设置
++++++++++++++

预先设定所有传感器所用的网段（与HX，YG不同网段）

激光雷达

1. 将电脑IP设为和激光雷达默认IP同网段的IP
2. 通过电脑端sopas连接激光雷达，设置激光雷达IP为传感器网段


Hanxin网络设置
+++++++++++++

1. 为了保证网络质量，建议韩信使用网线连接到路由器上。

2. 如无法有线连接，则点击右上角网络图标连接wifi网络。


Yugong网络设置
+++++++++++++

无线网络

1. 点击右上角网络图标，选择wifi网络连接，默认自动获取IP。

2. 如需设定为固定IP，选择Edit Connections，选择相应的网络，点击右侧Edit

3. 打开IPv4 Settings标签页，Method下拉框选则Manual，Adresses点击右侧Add
   Address填入IP地址，Netmask24，Gateway为网关，DNS202.96.209.133，8.8.8.8，点击Save

有线网络

1. 点击右上角网络图标，打开Edit Connections，选择Wired connection 1，点击右侧Edit

2. 打开IPv4 Settings标签页，Method下拉框选则Manual，Adresses点击右侧Add，添加IP地址（与传感器网段相同）后点击Save

3. 打开下方Routes选项，勾选‘Use this connection only for resources on its network’



