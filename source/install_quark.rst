Quark Installation
------------------

Quark Installer
+++++++++++++++

Download the :download:`quark_installer.sh <howto/quark_installer.sh>`.


Install on IPC
++++++++++++++

1. Open system settings --> Brightness & Lock --> switch off "Lock"

2. Open a terminal window.

3. Run the quark_installer.sh script, and type in system password.

::

    $ source ~/Downloads/install_quark.sh


4. Press `h` for `hanxin`, `y` for `yugong`, and `n` for `nami`. If you want to install all the source code, please choose dev version, by pressing `y`.

5. Bitbucket account info is required for once.

6. Wait for a cup of coffee or tea.

FAQ
+++
TODO: Sai
