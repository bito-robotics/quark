#!/bin/bash

set -o errexit

sudo rm -f /var/lib/dpkg/lock
sudo rm -f /var/lib/apt/lists/lock

sudo apt -y install git

git config --global credential.helper 'cache --timeout 9600'
# global info git 
echo " "
echo " "
echo 'Enter your name for "git config --global user.name", followd by [ENTER]:'
echo " "
read name
git config --global --replace-all user.name  $name
echo " "
echo 'Enter your email address for "git config --global user.email", followd by [ENTER]:'
echo " "
read email  
git config --global --replace-all user.email $email

while true; do
    read -p "what is this computer used for? [h] for hanxin, [y] for yugong, [n] for nami: " device
    case $device in
        [Hh]* ) read -p "Do you want to install developer version? [y/n]: " dev
                case $dev in 
                    [Yy]* ) echo "install hanxin developer version: "
                               git clone https://bitbucket.org/bito-robotics/hanxin_installer.git
                               chmod +x ~/hanxin_installer/hanxin_installer_dev.sh;
                               ./hanxin_installer/hanxin_installer_dev.sh;
                               break;;
                    [Nn]* ) echo "install hanxin release version: "
                               git clone https://bitbucket.org/bito-robotics/hanxin_installer.git
                               chmod +x ~/hanxin_installer/hanxin_installer.sh;
                               ./hanxin_installer/hanxin_installer.sh;
                               break;;
                    * ) echo "Do you want to install developer version? [y/n]: ";;
                esac
                break;;
        [Nn]* ) read -p "Do you want to install developer version? [y/n]: " dev
                case $dev in 
                    [Yy]* ) echo "install nami developer version: "
                               git clone https://bitbucket.org/bito-robotics/nami_installer.git
                               chmod +x ~/nami_installer/nami_installer_dev.sh;
                               ./nami_installer/nami_installer_dev.sh;
                               break;;
                    [Nn]* ) echo "install nami release version: "
                               git clone https://bitbucket.org/bito-robotics/nami_installer.git
                               chmod +x ~/nami_installer/nami_installer.sh;
                               ./nami_installer/nami_installer.sh;
                               break;;
                    * ) echo "Do you want to install developer version? [y/n]: ";;
                esac
                break;;
        [Yy]* ) read -p "Do you want to install developer version? [y/n]: " dev
                case $dev in 
                    [Yy]* ) echo "install yugong developer version: "
                               git clone https://bitbucket.org/bito-robotics/yugong_installer.git
                               chmod +x ~/yugong_installer/yugong_installer_dev.sh;
                               ./yugong_installer/yugong_installer_dev.sh;
                               break;;
                    [Nn]* ) echo "install yugong release version: "
                               git clone https://bitbucket.org/bito-robotics/yugong_installer.git
                               chmod +x ~/yugong_installer/yugong_installer.sh;
                               ./yugong_installer/yugong_installer.sh;
                               break;;
                    * ) echo "Do you want to install developer version? [y/n]: ";;
                esac
                break;;
        * ) echo "what is this computer used for? [h] for hanxin, [y] for yugong, [n] for nami: ";;
    esac
done