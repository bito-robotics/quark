相机外参标定
------------

参数说明
++++++++
相机外参（英文：`camera extrinsics`），指相机光学中心所在机器人坐标系下的相对位置。

标定配件
++++++++
蓝蚂蚁机器人标定摄像头外参需搭配专用的 :download:`二维码地毯 <_static/calibration_mat.pdf>` 。

标定步骤(若未更新外参标定的软件代码，则只需步骤1、2)
++++++++
1. 将蓝蚂蚁机器人用手柄遥控到二维码地毯上，并精准停到标线内，前后轮对准刻度线。建议让机器人从地毯的侧面或前面驶入，避免轮子压到二维码区域，影响精度。

.. figure:: _static/calibration/cam_ext_cal_1.JPG
   :width: 60%
   :align: center
   :figclass: align-

.. figure:: _static/calibration/cam_ext_cal_2.JPG
   :width: 60%
   :align: center
   :figclass: align-centered

2. 将贴有标识二维码的货架摆放的二维码地毯上，货架腿精确摆放在四个方框中，标识二维码正对前视摄像头。

.. figure:: _static/calibration/cam_ext_cal_3.JPG
   :width: 60%
   :align: center
   :figclass: align-centered

3. 打开命令行1,运行标定脚本。

::

    $ roslaunch yugong blue_ant_bring_up.launch


4. 打开一个命令行2。

::

    $ rqt_image_view


.. figure:: _static/calibration/cam_ext_cal_4.png
   :width: 60%
   :align: center
   :figclass: align-

显示图片后点击图片红色框处选择/camera1/image这个话题，如下图所示

.. figure:: _static/calibration/cam_ext_cal_5.png
   :width: 60%
   :align: center
   :figclass: align-


5. 打开命令行3

::

    $ rostopic echo /ygxxxxxx/apriltag/detections_servo

此时可以看到图像，识别到的二维码会显示在屏幕上

6. 打开命令行4,(pg_123124中的数字是前置相机序列号，int_data里的[10]是百分之10的亮度值，这个值是0到100间。越大越亮，）

::

    $ rosservice call /ygxxxxxxx/pg_123124/set_cam_param "param_name: 'brightness' id: 0 param_flag: [3] double data: [0] int_data: [10]"

.. figure:: _static/calibration/cam_ext_cal_6.png
   :width: 60%
   :align: center
   :figclass: align-

7. 打开命令行5(或ssh到机器人上)。

::

    $ roslaunch foot_eye_calibrator foot_eye_calibration.launch


8. 检查标定数据。

::

    $ roscd yugong/data/tag_map
    $ vim cam.txt

对于蓝蚂蚁机器人，正确的摄像头参数约为下框。

::

    cam_id: 0 extrinsics: -1.74 -1.74 0.71 -0.91 0.01 0.68
    cam_id: 1 extrinsics: -1.20 1.32 -1.26 -0.71 0.01 0.32

9. 重启机器人。
