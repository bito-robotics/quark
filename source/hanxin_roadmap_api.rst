路网API
----------


创建路网
~~~~~~~~

**简要描述：**

-  创建一个路网

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/roadmap/create_roadmap/``

**请求方式：**

-  POST

**请求示例：**

-  ``http://192.168.124.225:5000/api/v1/roadmap/create_roadmap``

**请求参数：** json

.. code:: json

    {
        "table_name": "dandan",
        "nodes": [
            {
                "node_id": 1,
                "x": 12,
                "y": 25,
                "theta": "3.14",
                "logical_pose": ""
            },
            {
                "node_id": 2,
                "x": 9,
                "y": 25,
                "theta": "3.14",
                "logical_pose": "h2"
            },
            {
                "node_id": 3,
                "x": 12,
                "y": 5,
                "theta": "3.14",
                "logical_pose": ""
            },
            {
                "node_id": 4,
                "x": 2,
                "y": 5,
                "theta": "3.14",
                "logical_pose": "h1"
            }
            ],
        "edges": [
            {
                "source": 1,
                "target": 2
            },
            {
                "source": 2,
                "target": 4
            }
            ]
    }

**请求参数：**

+-----------------+--------+----------+------------------+
| 参数名          | 必选   | 类型     | 说明             |
+=================+========+==========+==================+
| table\_name     | 是     | string   | 新建路网表名     |
+-----------------+--------+----------+------------------+
| node\_id        | 是     | int      | 节点id           |
+-----------------+--------+----------+------------------+
| x               | 是     | int      | 节点x坐标        |
+-----------------+--------+----------+------------------+
| y               | 是     | int      | 节点y坐标        |
+-----------------+--------+----------+------------------+
| theta           | 是     | string   | 节点角度         |
+-----------------+--------+----------+------------------+
| logical\_pose   | 否     | string   | 逻辑库位名       |
+-----------------+--------+----------+------------------+
| source          | 是     | int      | 路径起始节点id   |
+-----------------+--------+----------+------------------+
| target          | 是     | int      | 路径到达节点id   |
+-----------------+--------+----------+------------------+

**成功返回示例：** json

.. code:: json

    {
        "errmsg": "Create_OK",
        "errno": "0",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+----------------+
| 参数名        | 类型     | 说明           |
+===============+==========+================+
| table\_name   | string   | 新建路网表名   |
+---------------+----------+----------------+
| errno         | string   | API错误码      |
+---------------+----------+----------------+
| errmsg        | string   | 错误信息       |
+---------------+----------+----------------+

**错误返回示例：** json

.. code:: json

    {
        "errmsg": "Create_Fail",
        "errno": "4001",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+----------------+
| 参数名        | 类型     | 说明           |
+===============+==========+================+
| table\_name   | string   | 新建路网表名   |
+---------------+----------+----------------+
| errno         | string   | API错误码      |
+---------------+----------+----------------+
| errmsg        | string   | 错误信息       |
+---------------+----------+----------------+


加载路网
~~~~~~~~

**简要描述：**

-  加载某个路网

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/roadmap/load_roadmap/<table_name>``

**请求方式：**

-  GET

**请求示例：**

-  ``http://192.168.124.225:5000/api/v1/roadmap/load_roadmap/roadmap_node_table_dandan``

**请求参数：**

无

**请求参数说明：**

+---------------+--------+----------+--------------------+
| 参数名        | 必选   | 类型     | 说明               |
+===============+========+==========+====================+
| table\_name   | 是     | string   | 要加载的路网表名   |
+---------------+--------+----------+--------------------+

**成功返回示例：** json

.. code:: json

    {
        "errmsg": "Load_OK",
        "errno": "0",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+--------------------+
| 参数名        | 类型     | 说明               |
+===============+==========+====================+
| table\_name   | string   | 要加载的路网表名   |
+---------------+----------+--------------------+
| errno         | string   | API错误码          |
+---------------+----------+--------------------+
| errmsg        | string   | 错误信息           |
+---------------+----------+--------------------+

**错误返回示例：** json

.. code:: json

    {
        "errmsg": "Load_Fail",
        "errno": "4001",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+--------------------+
| 参数名        | 类型     | 说明               |
+===============+==========+====================+
| table\_name   | string   | 要加载的路网表名   |
+---------------+----------+--------------------+
| errno         | string   | API错误码          |
+---------------+----------+--------------------+
| errmsg        | string   | 错误信息           |
+---------------+----------+--------------------+

添加路网节点
~~~~~~~~~~~~

**简要描述：**

-  添加一个或者多个路网节点

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/roadmap/add_nodes``

**请求方式：**

-  POST

**请求示例：**

-  ``http://192.168.124.225:5000/api/v1/roadmap/add_nodes``

**请求参数：** json

.. code:: json

    {
        "table_name": "roadmap_node_table_dandan",
        "nodes": [
            {
                "x":22.37,
                "y":20.44,
                "theta": 2.1
            },
            {
                "x":27.37,
                "y":10.44,
                "theta": 2.1
            }
            ]
    }

**请求参数说明：**

+---------------+--------+----------+---------------------------------------+
| 参数名        | 必选   | 类型     | 说明                                  |
+===============+========+==========+=======================================+
| table\_name   | 是     | string   | 要添加节点的路网表名                  |
+---------------+--------+----------+---------------------------------------+
| x             | 是     | float    | 路网节点x坐标                         |
+---------------+--------+----------+---------------------------------------+
| y             | 是     | float    | 路网节点y坐标                         |
+---------------+--------+----------+---------------------------------------+
| theta         | 是     | float    | 路网节点theta坐标，表示该节点的方向   |
+---------------+--------+----------+---------------------------------------+

**成功返回示例：** json

.. code:: json

    {
        "errmsg": "Add_OK",
        "errno": "0",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+------------------------+
| 参数名        | 类型     | 说明                   |
+===============+==========+========================+
| table\_name   | string   | 要添加节点的路网表名   |
+---------------+----------+------------------------+
| errno         | string   | API错误码              |
+---------------+----------+------------------------+
| errmsg        | string   | 错误信息               |
+---------------+----------+------------------------+

**错误返回示例：** json

.. code:: json

    {
        "errmsg": "Add_Fail",
        "errno": "4001",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+------------------------+
| 参数名        | 类型     | 说明                   |
+===============+==========+========================+
| table\_name   | string   | 要添加节点的路网表名   |
+---------------+----------+------------------------+
| errno         | string   | API错误码              |
+---------------+----------+------------------------+
| errmsg        | string   | 错误信息               |
+---------------+----------+------------------------+

添加路网通道
~~~~~~~~~~~~

**简要描述：**

-  添加一个或者多个路网通道

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/roadmap/add_edges``

**请求方式：**

-  POST

**请求示例：**

-  ``http://192.168.124.225:5000/api/v1/roadmap/add_edges``

**请求参数：** json

.. code:: json

    {
        "table_name": "roadmap_node_table_dandan",
        "edges": [
            {
                "source":1,
                "target":5
            },
            {
                "source":2,
                "target":3
            }
            ]
    }

**请求参数说明：**

+---------------+--------+----------+------------------------------+
| 参数名        | 必选   | 类型     | 说明                         |
+===============+========+==========+==============================+
| table\_name   | 是     | string   | 要添加的路网通道的路网表名   |
+---------------+--------+----------+------------------------------+
| source        | 是     | int      | 路网通道的起始节点id号       |
+---------------+--------+----------+------------------------------+
| target        | 是     | int      | 路网通道的终止节点id号       |
+---------------+--------+----------+------------------------------+

**成功返回示例：** json

.. code:: json

    {
        "errmsg": "Add_OK",
        "errno": "0",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+------------------------------+
| 参数名        | 类型     | 说明                         |
+===============+==========+==============================+
| table\_name   | string   | 要添加的路网通道的路网表名   |
+---------------+----------+------------------------------+
| errno         | string   | API错误码                    |
+---------------+----------+------------------------------+
| errmsg        | string   | 错误信息                     |
+---------------+----------+------------------------------+

**错误返回示例：** json

.. code:: json

    {
        "errmsg": "Add_Fail",
        "errno": "4001",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+------------------------------+
| 参数名        | 类型     | 说明                         |
+===============+==========+==============================+
| table\_name   | string   | 要添加的路网通道的路网表名   |
+---------------+----------+------------------------------+
| errno         | string   | API错误码                    |
+---------------+----------+------------------------------+
| errmsg        | string   | 错误信息                     |
+---------------+----------+------------------------------+

添加路网逻辑库位
~~~~~~~~~~~~~~~~

**简要描述：**

-  添加一个或者多个路网节点

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/roadmap/add_logics``

**请求方式：**

-  POST

**请求示例：**

-  ``http://192.168.124.225:5000/api/v1/roadmap/add_logics``

**请求参数：** json

.. code:: json

    {
        "table_name": "roadmap_node_table_dandan",
        "logics": [
            {
                "logical_pose":"A3",
                "x":12,
                "y":25,
                "theta":3.14,
                "node_id":1
            },
            {
                "logical_pose":"A4",
                "x":4.39,
                "y":23.68,
                "theta":0,
                "node_id":5
            }
            ]
    }

**请求参数说明：**

+---------------+--------+----------+---------------------------------------------------+
| 参数名        | 必选   | 类型     | 说明                                              |
+===============+========+==========+===================================================+
| table\_name   | 是     | string   | 要添加逻辑库位的路网表名                          |
+---------------+--------+----------+---------------------------------------------------+
| x             | 是     | float    | 路网真实逻辑库位节点x坐标                         |
+---------------+--------+----------+---------------------------------------------------+
| y             | 是     | float    | 路网真实逻辑库位节点y坐标                         |
+---------------+--------+----------+---------------------------------------------------+
| theta         | 是     | float    | 路网真实逻辑库位节点theta坐标，表示该节点的方向   |
+---------------+--------+----------+---------------------------------------------------+
| node\_id      | 是     | int      | 路网真实逻辑库位节点id号                          |
+---------------+--------+----------+---------------------------------------------------+

**成功返回示例：** json

.. code:: json

    {
        "errmsg": "Add_OK",
        "errno": "0",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+----------------------------+
| 参数名        | 类型     | 说明                       |
+===============+==========+============================+
| table\_name   | string   | 要添加逻辑库位的路网表名   |
+---------------+----------+----------------------------+
| errno         | string   | API错误码                  |
+---------------+----------+----------------------------+
| errmsg        | string   | 错误信息                   |
+---------------+----------+----------------------------+

**错误返回示例：** json

.. code:: json

    {
        "errmsg": "Add_Fail",
        "errno": "4001",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+----------------------------+
| 参数名        | 类型     | 说明                       |
+===============+==========+============================+
| table\_name   | string   | 要添加逻辑库位的路网表名   |
+---------------+----------+----------------------------+
| errno         | string   | API错误码                  |
+---------------+----------+----------------------------+
| errmsg        | string   | 错误信息                   |
+---------------+----------+----------------------------+

删除路网
~~~~~~~~

**简要描述：**

-  删除某个路网

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/roadmap/delete_roadmap``

**请求方式：**

-  DELETE

**请求示例：**

-  ``http://192.168.124.225:5000/api/v1/roadmap/delete_roadmap``

**请求参数：** json

.. code:: json

    {
        "table_name": "roadmap_node_table_dandan"
    }

**请求参数说明：**

+---------------+--------+----------+--------------------+
| 参数名        | 必选   | 类型     | 说明               |
+===============+========+==========+====================+
| table\_name   | 是     | string   | 要加载的路网表名   |
+---------------+--------+----------+--------------------+

**成功返回示例：** json

.. code:: json

    {
        "errmsg": "Del_OK",
        "errno": "0",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+--------------------+
| 参数名        | 类型     | 说明               |
+===============+==========+====================+
| table\_name   | string   | 要删除的路网表名   |
+---------------+----------+--------------------+
| errno         | string   | API错误码          |
+---------------+----------+--------------------+
| errmsg        | string   | 错误信息           |
+---------------+----------+--------------------+

**错误返回示例：** json

.. code:: json

    {
        "errmsg": "Del_OK",
        "errno": "4001",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+--------------------+
| 参数名        | 类型     | 说明               |
+===============+==========+====================+
| table\_name   | string   | 要删除的路网表名   |
+---------------+----------+--------------------+
| errno         | string   | API错误码          |
+---------------+----------+--------------------+
| errmsg        | string   | 错误信息           |
+---------------+----------+--------------------+

删除路网节点
~~~~~~~~~~~~

**简要描述：**

-  删除某个节点

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/roadmap/delete_node``

**请求方式：**

-  DELETE

**请求示例：**

-  ``http://192.168.124.225:5000/api/v1/roadmap/delete_node``

**请求参数：** json

.. code:: json

    {
        "table_name": "roadmap_node_table_dandan",
        "node_id": 4
    }

**请求参数说明：**

+---------------+--------+----------+------------------------+
| 参数名        | 必选   | 类型     | 说明                   |
+===============+========+==========+========================+
| table\_name   | 是     | string   | 要删除节点的路网表名   |
+---------------+--------+----------+------------------------+
| node\_id      | 是     | int      | 节点id                 |
+---------------+--------+----------+------------------------+

**成功返回示例：** json

.. code:: json

    {
        "errmsg": "Del_OK",
        "errno": "0",
        "node_id": 4,
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+------------------------------+
| 参数名        | 类型     | 说明                         |
+===============+==========+==============================+
| table\_name   | string   | 要删除的路网节点的路网表名   |
+---------------+----------+------------------------------+
| node\_id      | int      | 路网节点id                   |
+---------------+----------+------------------------------+
| errno         | string   | API错误码                    |
+---------------+----------+------------------------------+
| errmsg        | string   | 错误信息                     |
+---------------+----------+------------------------------+

**错误返回示例：** json

.. code:: json

    {
        "errmsg": "Del_Fail",
        "errno": "4001",
        "node_id": 4,
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+------------------------------+
| 参数名        | 类型     | 说明                         |
+===============+==========+==============================+
| table\_name   | string   | 要删除的路网节点的路网表名   |
+---------------+----------+------------------------------+
| node\_id      | int      | 路网节点id                   |
+---------------+----------+------------------------------+
| errno         | string   | API错误码                    |
+---------------+----------+------------------------------+
| errmsg        | string   | 错误信息                     |
+---------------+----------+------------------------------+

删除路网通道
~~~~~~~~~~~~

**简要描述：**

-  删除某个路网通道

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/roadmap/delete_edge``

**请求方式：**

-  DELETE

**请求示例：**

-  ``http://192.168.124.225:5000/api/v1/roadmap/delete_edge``

**请求参数：** json

.. code:: json

    {
        "table_name": "roadmap_node_table_dandan",
        "edge_id": 2
    }

**请求参数说明：**

+---------------+--------+----------+------------------------------+
| 参数名        | 必选   | 类型     | 说明                         |
+===============+========+==========+==============================+
| table\_name   | 是     | string   | 要删除的路网通道的路网表名   |
+---------------+--------+----------+------------------------------+
| edge\_id      | 是     | int      | 路网通道的id                 |
+---------------+--------+----------+------------------------------+

**成功返回示例：** json

.. code:: json

    {
        "errmsg": "Del_OK",
        "errno": "0",
        "edge_id": 2,
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+------------------------------+
| 参数名        | 类型     | 说明                         |
+===============+==========+==============================+
| table\_name   | string   | 要删除的路网通道的路网表名   |
+---------------+----------+------------------------------+
| edge\_id      | int      | 路网通道的id                 |
+---------------+----------+------------------------------+
| errno         | string   | API错误码                    |
+---------------+----------+------------------------------+
| errmsg        | string   | 错误信息                     |
+---------------+----------+------------------------------+

**错误返回示例：** json

.. code:: json

    {
        "errmsg": "Del_Fail",
        "errno": "4001",
        "edge_id": 2,
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+------------------------------+
| 参数名        | 类型     | 说明                         |
+===============+==========+==============================+
| table\_name   | string   | 要删除的路网通道的路网表名   |
+---------------+----------+------------------------------+
| edge\_id      | int      | 路网通道的id                 |
+---------------+----------+------------------------------+
| errno         | string   | API错误码                    |
+---------------+----------+------------------------------+
| errmsg        | string   | 错误信息                     |
+---------------+----------+------------------------------+

删除路网逻辑库位
~~~~~~~~~~~~~~~~

**简要描述：**

-  删除某个路网的某个逻辑库位

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/roadmap/delete_logic``

**请求方式：**

-  DELETE

**请求示例：**

-  ``http://192.168.124.225:5000/api/v1/roadmap/delete_logic``

**请求参数：** json

.. code:: json

    {
        "table_name": "roadmap_node_table_dandan",
        "logical_pose": "h2"
    }

**请求参数说明：**

+---------------+--------+----------+------------------------------+
| 参数名        | 必选   | 类型     | 说明                         |
+===============+========+==========+==============================+
| table\_name   | 是     | string   | 要删除的逻辑库位的路网表名   |
+---------------+--------+----------+------------------------------+
| logical\_pose | 是     | int      | 逻辑库位名                   |
+---------------+--------+----------+------------------------------+

**成功返回示例：** json

.. code:: json

    {
        "errmsg": "Del_OK",
        "errno": "0",
        "logical_pose": "h2",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+------------------------------+
| 参数名        | 类型     | 说明                         |
+===============+==========+==============================+
| table\_name   | string   | 要删除的逻辑库位的路网表名   |
+---------------+----------+------------------------------+
| logical\_pose | int      | 逻辑库位名                   |
+---------------+----------+------------------------------+
| errno         | string   | API错误码                    |
+---------------+----------+------------------------------+
| errmsg        | string   | 错误信息                     |
+---------------+----------+------------------------------+

**错误返回示例：** json

.. code:: json

    {
        "errmsg": "Del_Fail",
        "errno": "4001",
        "logical_pose": "h2",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+------------------------------+
| 参数名        | 类型     | 说明                         |
+===============+==========+==============================+
| table\_name   | string   | 要删除的逻辑库位的路网表名   |
+---------------+----------+------------------------------+
| logical\_pose | int      | 逻辑库位名                   |
+---------------+----------+------------------------------+
| errno         | string   | API错误码                    |
+---------------+----------+------------------------------+
| errmsg        | string   | 错误信息                     |
+---------------+----------+------------------------------+


修改逻辑库位名
~~~~~~~~~~~~~~

**简要描述：**

-  修改某个逻辑库位名

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/road_map/update_logic_name``

**请求方式：**

-  PUT

**请求示例：**

-  ``http://192.168.124.225:5000/api/v1/roadmap/update_logic_name``

**请求参数：** json

.. code:: json

    {
        "table_name": "roadmap_node_table_dandan",
        "old_logical_pose": "h2",
        "new_logical_pose": "H8"
    }

**请求参数说明：**

+----------------------+--------+----------+--------------------------------+
| 参数名               | 必选   | 类型     | 说明                           |
+======================+========+==========+================================+
| table\_name          | 是     | string   | 要修改的逻辑库位名的路网表名   |
+----------------------+--------+----------+--------------------------------+
| old\_logical\_pose   | 是     | string   | 原逻辑库位名                   |
+----------------------+--------+----------+--------------------------------+
| new\_logical\_pose   | 是     | string   | 新逻辑库位名                   |
+----------------------+--------+----------+--------------------------------+

**成功返回示例：** json

.. code:: json

    {
        "errmsg": "Update_OK",
        "errno": "0",
        "new_logical_pose": "H8",
        "old_logical_pose": "h2",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+----------------------+----------+--------------------------------+
| 参数名               | 类型     | 说明                           |
+======================+==========+================================+
| table\_name          | string   | 要修改的逻辑库位名的路网表名   |
+----------------------+----------+--------------------------------+
| old\_logical\_pose   | string   | 原逻辑库位名                   |
+----------------------+----------+--------------------------------+
| new\_logical\_pose   | string   | 新逻辑库位名                   |
+----------------------+----------+--------------------------------+
| errno                | string   | API错误码                      |
+----------------------+----------+--------------------------------+
| errmsg               | string   | 错误信息                       |
+----------------------+----------+--------------------------------+

**错误返回示例：** json

.. code:: json

    {
        "errmsg": "Update_Fail",
        "errno": "4001",
        "new_logical_pose": "H8",
        "old_logical_pose": "h2",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+----------------------+----------+--------------------------------+
| 参数名               | 类型     | 说明                           |
+======================+==========+================================+
| table\_name          | string   | 要修改的逻辑库位名的路网表名   |
+----------------------+----------+--------------------------------+
| old\_logical\_pose   | string   | 原逻辑库位名                   |
+----------------------+----------+--------------------------------+
| new\_logical\_pose   | string   | 新逻辑库位名                   |
+----------------------+----------+--------------------------------+
| errno                | string   | API错误码                      |
+----------------------+----------+--------------------------------+
| errmsg               | string   | 错误信息                       |
+----------------------+----------+--------------------------------+

修改路网节点
~~~~~~~~~~~~

**简要描述：**

-  修改某个路网节点

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/road_map/update_node``

**请求方式：**

-  PUT

**请求示例：**

-  ``http://192.168.124.225:5000/api/v1/roadmap/update_node``

**请求参数：** json

.. code:: json

    {
        "table_name": "roadmap_node_table_dandan",
        "node_id": 2,
        "data": {
            "x": 18,
            "y":20
        }
    }

或者

.. code:: json

    {
        "table_name": "roadmap_node_table_dandan",
        "node_id": 2,
        "data": {
            "x": 18,
            "y":20,
            "theta": 3.14,
            "cost": 2.1,
            "type": 1
        }
    }

**请求参数说明：**

+---------------+--------+----------+-----------------------------------+
| 参数名        | 必选   | 类型     | 说明                              |
+===============+========+==========+===================================+
| table\_name   | 是     | string   | 要修改的路网节点的路网表名        |
+---------------+--------+----------+-----------------------------------+
| node\_id      | 是     | int      | 路网节点id                        |
+---------------+--------+----------+-----------------------------------+
| x             | 否     | float    | 路网节点的x坐标                   |
+---------------+--------+----------+-----------------------------------+
| y             | 否     | float    | 路网节点的y坐标                   |
+---------------+--------+----------+-----------------------------------+
| theta         | 否     | float    | 路网节点的theta角度，节点的方向   |
+---------------+--------+----------+-----------------------------------+
| cost          | 否     | float    | 路网节点成本                      |
+---------------+--------+----------+-----------------------------------+
| type          | 否     | int      | 路网节点的类型（暂时无用）        |
+---------------+--------+----------+-----------------------------------+

**成功返回示例：** json

.. code:: json

    {
        "errmsg": "Update_OK",
        "errno": "0",
        "node_id": 2,
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+------------------------------+
| 参数名        | 类型     | 说明                         |
+===============+==========+==============================+
| table\_name   | string   | 要修改的路网节点的路网表名   |
+---------------+----------+------------------------------+
| node\_id      | int      | 路网节点id                   |
+---------------+----------+------------------------------+
| errno         | string   | API错误码                    |
+---------------+----------+------------------------------+
| errmsg        | string   | 错误信息                     |
+---------------+----------+------------------------------+

**错误返回示例：** json

.. code:: json

    {
        "errmsg": "Update_Fail",
        "errno": "0",
        "node_id": 2,
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+------------------------------+
| 参数名        | 类型     | 说明                         |
+===============+==========+==============================+
| table\_name   | string   | 要修改的路网节点的路网表名   |
+---------------+----------+------------------------------+
| node\_id      | int      | 路网节点id                   |
+---------------+----------+------------------------------+
| errno         | string   | API错误码                    |
+---------------+----------+------------------------------+
| errmsg        | string   | 错误信息                     |
+---------------+----------+------------------------------+


查询所有路网表名
~~~~~~~~~~~~~~~~

**简要描述：**

-  查询所有的路网表名列表

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/roadmap/get_roadmap_list``

**请求方式：**

-  GET

**请求示例：**

-  ``http://192.168.124.225:5000/api/v1/roadmap/get_roadmap_list``

**请求参数：**

无

**成功返回示例：** json

.. code:: json

    {
        "data": {
            "road_map_lst": [
                "roadmap_node_table_0",
                "roadmap_node_table_1",
                "roadmap_node_table_1114",
                "roadmap_node_table_1114_2",
                "roadmap_node_table_1116_cc",
                "roadmap_node_table_2",
                "roadmap_node_table_3",
                "roadmap_node_table_3d_6",
                "roadmap_node_table_3d_zhuzaochang",
                "roadmap_node_table_4",
                "roadmap_node_table_5",
                "roadmap_node_table_6",
                "roadmap_node_table_7",
                "roadmap_node_table_8",
                "roadmap_node_table_9",
                "roadmap_node_table_dandan",
                "roadmap_node_table_zizhu_pv_road"
            ]
        },
        "errmsg": "Query_OK",
        "errno": "0"
    }

**返回参数说明：**

+------------------+----------+--------------------+
| 参数名           | 类型     | 说明               |
+==================+==========+====================+
| road\_map\_lst   | list     | 所有路网表名集合   |
+------------------+----------+--------------------+
| errno            | string   | API错误码          |
+------------------+----------+--------------------+
| errmsg           | string   | 错误信息           |
+------------------+----------+--------------------+

**错误返回示例：** json

.. code:: json

    {
        "errmsg": "Query_Fail",
        "errno": "4001"
    }

**返回参数说明：**

+------------------+----------+--------------------+
| 参数名           | 类型     | 说明               |
+==================+==========+====================+
| road\_map\_lst   | list     | 所有路网表名集合   |
+------------------+----------+--------------------+
| errno            | string   | API错误码          |
+------------------+----------+--------------------+
| errmsg           | string   | 错误信息           |
+------------------+----------+--------------------+


查询路网详情
~~~~~~~~~~~~

**简要描述：**

-  查询某个路网详细信息

**请求URL：**

-  ``http://<IP>:<PORT>/api/v1/roadmap/get_roadmap/<table_name>``

**请求方式：**

-  GET

**请求示例：**

-  ``http://192.168.124.225:5000/api/v1/roadmap/get_roadmap/roadmap_node_table_dandan``

**请求参数：**

无

**请求参数说明：**

+---------------+--------+----------+--------------------+
| 参数名        | 必选   | 类型     | 说明               |
+===============+========+==========+====================+
| table\_name   | 是     | string   | 要查看的路网表名   |
+---------------+--------+----------+--------------------+

**成功返回示例：** json

.. code:: json

    {
        "data": {
            "edges": [
                {
                    "cost": -1,
                    "edge_id": 1,
                    "source": 1,
                    "target": 4,
                    "type": 0
                },
                {
                    "cost": -1,
                    "edge_id": 2,
                    "source": 2,
                    "target": 4,
                    "type": 0
                }
            ],
            "logicals": [
                {
                    "id": 1,
                    "logical_pose": "h2",
                    "real_pose_id": 2
                },
                {
                    "id": 2,
                    "logical_pose": "h1",
                    "real_pose_id": 4
                }
            ],
            "nodes": [
                {
                    "cost": 0,
                    "node_id": 1,
                    "status": 1,
                    "theta": 3.14,
                    "type": 0,
                    "x": 1,
                    "y": 25
                },
                {
                    "cost": 0,
                    "node_id": 2,
                    "status": 1,
                    "theta": 3.14,
                    "type": 0,
                    "x": 10,
                    "y": 25
                },
                {
                    "cost": 0,
                    "node_id": 3,
                    "status": 1,
                    "theta": 3.14,
                    "type": 0,
                    "x": 12,
                    "y": 5
                },
                {
                    "cost": 0,
                    "node_id": 4,
                    "status": 1,
                    "theta": 3.14,
                    "type": 0,
                    "x": 2,
                    "y": 5
                }
            ]
        },
        "errmsg": "Query_OK",
        "errno": "0",
        "table_name": "roadmap_node_table_dandan"

**返回参数说明：**

+------------+---------+--------------------------------------------+
| 参数名     | 类型    | 说明                                       |
+============+=========+============================================+
| nodes      | list    | 路网节点集合                               |
+------------+---------+--------------------------------------------+
| node\_id   | int     | 路网节点id                                 |
+------------+---------+--------------------------------------------+
| x          | float   | 路网节点x坐标                              |
+------------+---------+--------------------------------------------+
| y          | float   | 路网节点y坐标                              |
+------------+---------+--------------------------------------------+
| theta      | float   | 路网节点theta坐标，表示该节点的方向        |
+------------+---------+--------------------------------------------+
| cost       | float   | 路网节点成本                               |
+------------+---------+--------------------------------------------+
| type       | int     | 路网节点类型（暂时无用）                   |
+------------+---------+--------------------------------------------+
| status     | int     | 路网节点被占据（=1），路网节点空闲（=0）   |
+------------+---------+--------------------------------------------+

+------------+---------+----------------------------+
| 参数名     | 类型    | 说明                       |
+============+=========+============================+
| edges      | list    | 路网通道集合               |
+------------+---------+----------------------------+
| edge\_id   | int     | 路网通道id                 |
+------------+---------+----------------------------+
| source     | int     | 路网通道的起始节点id号     |
+------------+---------+----------------------------+
| target     | int     | 路网通道的终止节点id号     |
+------------+---------+----------------------------+
| cost       | float   | 路网通道的成本             |
+------------+---------+----------------------------+
| type       | int     | 路网通道类型（暂时无用）   |
+------------+---------+----------------------------+

+------------------+----------+--------------------------------+
| 参数名           | 类型     | 说明                           |
+==================+==========+================================+
| logicals         | list     | 逻辑库位集合                   |
+------------------+----------+--------------------------------+
| id               | int      | 逻辑库位id                     |
+------------------+----------+--------------------------------+
| logical\_pose    | string   | 逻辑库位名（不允许重复）       |
+------------------+----------+--------------------------------+
| real\_pose\_id   | int      | 逻辑库位真实位置路网节点id号   |
+------------------+----------+--------------------------------+
| table\_name      | string   | 要加载的路网表名               |
+------------------+----------+--------------------------------+
| errno            | string   | API错误码                      |
+------------------+----------+--------------------------------+
| errmsg           | string   | 错误信息                       |
+------------------+----------+--------------------------------+

**错误返回示例：** json

.. code:: json

    {
        "errmsg": "Query_Fail",
        "errno": "4001",
        "table_name": "roadmap_node_table_dandan"
    }

**返回参数说明：**

+---------------+----------+--------------------+
| 参数名        | 类型     | 说明               |
+===============+==========+====================+
| table\_name   | string   | 要查看的路网表名   |
+---------------+----------+--------------------+
| errno         | string   | API错误码          |
+---------------+----------+--------------------+
| errmsg        | string   | 错误信息           |
+---------------+----------+--------------------+