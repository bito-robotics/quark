Safety
======

Safety is extremely important to BITO Robotics. Safe operation of
robots is important yet challenging and it is important to remember
that safety is a continual process that is shared by the robot
designer, operator, and administrator. The following section provides
an overview of the issues, safety-related design features, and a basic
set of guidelines to support safety when using the Quark
R&D robots.