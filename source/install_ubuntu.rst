Ubuntu Installation
-------------------

Ubuntu 16.04
++++++++++++

`Quark` is developed on Linux Ubuntu 16.04 operating system. 
An official tutorial to create a bootable USB stick on Windows is linked: https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-windows#0

For more information about Ubuntu, see Ubuntu for Desktop web site: https://www.ubuntu.com/desktop.


Install on IPC
++++++++++++++

1. Type F2 during the system boot process to enter the BIOS settings. It is recommended that you disable Quick Boot and Quiet Boot in the BIOS to make it easier to catch any issues in the boot process. TODO: select poweron after power failure, to be confirmed by DJ Zhou.

2. Select language to English.

3. Connect to Internet via wifi or Ethernet.

4. Check `Download updates while installing ubuntu`, Uncheck `install third-party software`

5. Check `Erase disk and install Ubuntu`.

6. Choose `New York`.

7. User name and computer name format. TODO: Richard

8. On Hanxin, password: bitorobotics, and check `Require my password to log in`. On Yugong, password: chowiehoset, and uncheck `Require my password to log in`.

9. Wait for a cup of coffee or tea.

Dual Boot with Windows
++++++++++++++++++++++
TODO: Yang
