点云地图(中文版)
-------------

在线建图主要分为四大步骤

A. 配置激光建图软件参数并打开软件
B. 检查激光雷达等驱动设备是否正常工作
C. 设置rviz
D. 建立地图并保存

离线建图主要分为五大步骤

E. 录制bag(在线建图时可同步进行bag录制)
F. 配置激光建图软件参数并打开软件
G. 打开录制好的包并检查相关topic是否正常发布数据
H. 设置rviz
I. 建立地图并保存

定位手动开启主要分为三大步骤（默认开机自启）

J. 配置激光定位软件参数并打开软件
K. 检查激光雷达等驱动设备是否正常工作
L. 查看定位效果

A. 配置激光建图软件参数并打开软件
++++++++++++

1. 准备一台用于建图的电脑（已安装Bito建图软件），以下步骤使用Velodyne激光及Imu传感设备进行建图为例。
2. 打开激光建图参数配置文件,将use_imu_data设为true,lua文件参数修改后不需要重新编译。(有源码的参数配置文件目录为:~/yugong_ws/src/cartographer_ros/cartographer_ros/configuration_files/mapping_with_velodyne.lua 无源码的参数配置文件目录为：~/yugong_ws/install/share/cartographer_ros/configuration_files/mapping_with_velodyne.lua)

.. figure:: _static/lidar_mapping/mapping_with_velodyne_lua.png
   :width: 80%
   :align: center
   :figclass: align-

(注意: 不同的传感设备及参数的配置请详见 :download:`lua参数文件详解 <_static/lidar_mapping/lidar_mapping_configuration.pdf>`)。

3. 打开一个终端，输入以下命令。(ygxxxxxxx代表主机名)

::

	$ rosnode kill /ygxxxxxxx/cartographer_node
::

	$ source ~/yugong_ws/devel/setup.bash --extend
::

	$ roslaunch cartographer_ros mapping_by_velodyne.launch



B.检查激光雷达等驱动设备是否正常工作
++++++++++++

1. 输入以下命令查看topic列表，不同的设备topic的名字不一致:

::

	$ rostopic list


2. 在命令行中输入下面命令查看激光雷达等设备是否运行（注意：请事先确认使用了哪些设备，并逐一检查各个设备是否正常工作，例如本例子还使用了Imu）:

::

	$ rostopic echo /ygxxxxxxx/velodyne_points

::

	$  ^C (Ctrl+c)

::

	$ rostopic echo /imu(Imu的topic name无主机名前缀)

3. 如果激光雷达等设备未正常工作请输入以下命令打开(其他设备的开启请自行确认相应的驱动包，如Imu等):

::

	$ roslaunch velodyne_pointcloud VLP16_points.launch (Velodyne的驱动开启命令)

::

	$ roslaunch xsens_driver xsens_driver.launch (Imu的驱动开启命令)

(注意，Imu等USB接口的传感器，第一次安装使用时请先使用以下命令给USB接口权限)

::

	$  sudo chmod 777 /dev/ttyUSB*

C. 设置rviz
++++++++++++

1.　一般情况下，当运行步骤A的时候会就会弹出rviz的界面，但是当rviz文件没有跳出来的时候，运行下面命令。

::

	$ rosrun rviz rviz


2. 点击rviz的界面"ADD"添加所需的对象，比如观测路径则添加对应的path。

.. figure:: _static/lidar_mapping/add.png
   :width: 80%
   :align: center
   :figclass: align-


.. figure:: _static/lidar_mapping/topic_choose.png
   :width: 80%
   :align: center
   :figclass: align-

(注意，若所使用的机器人无显示器，则需要在mapping_by_velodyne.launch文件中关闭rviz，不然可能导致软件无法运行，launch文件目录为:~/yugong_ws/src/cartographer_ros/cartographer_ros/launch/mapping_by_velodyne.launch,操作与下图红色框内一样，将rviz注释掉)

.. figure:: _static/lidar_mapping/rviz_close.png
   :width: 80%
   :align: center
   :figclass: align-

D. 建立地图并保存
++++++++++++

1. 将机器人围着所需建图的场地走动（尽量让机器人在整个场地各个地方走一遍，起始点与终止点需要是同一个，建图过程中请录制相关的bag以供离线建图时使用，bag录制及离线建图步骤请详见下文）。

2. 打开一个新的命令框，运行以下命令。

::

	$ source ~/yugong_ws/devel/setup.bash --extend

::

	$ rosservice call "/ygxxxxxxx/finish_trajectory" "map"

::

	$ cd ~/.ros

::

	$ cp map.pcd ~/catkin_slam/pcd_map/map.pcd

（注意：若无catkin_slam目录，请在根目录下自行创建catkin_slam文件夹或是将~/.ros路径下的map.pcd文件存放在自定义位置，但是存放路径及pcd文件名请与localization_with_velodyne.lua的参数配置文件里保持一致。不然可能会导致使用定位软件的时候无法顺利找到所建地图并成功定位。有源码的参数配置文件目录为:~/yugong_ws/src/cartographer_ros/cartographer_ros/configuration_files/localization_with_velodyne.lua 无源码的参数配置文件目录为：~/yugong_ws/install/share/cartographer_ros/configuration_files/localization_with_velodyne.lua)

.. figure:: _static/lidar_mapping/lua.png
   :width: 80%
   :align: center
   :figclass: align-

3. 建图完成并存图后请及时关闭建图软件。

E. 录制bag(在线建图时可同步进行bag录制)
++++++++++++

1. 请使用以下命令查看所有的topic name

::

	$ rostopic list

2. 使用以下命令录制bag(建议录制所有所需的传感器的topic，本文以录制velodyne,imu及odom里程计为例)

::

	$ rosbag record /ygxxxxxxx/velodyne_points /imu /ygxxxxxxx/odom/wheel

3. 将机器人围着所需建图的场地走动（尽量让机器人在整个场地各个地方走一遍，起始点与终止点需要是同一个），录制完成后请及时使用以下命令关闭

::

	$ ^C (Ctrl+c)

F. 配置激光建图软件参数并打开软件
++++++++++++

1. 打开mapping_with_velodyne.lua文件进行参数配置，录制并使用了哪些传感器设备请在lua文件里进行相应的参数配置，本步骤与在线建图的参数配置步骤一致，请详见上文。

2. 打开mapping_by_velodyne.launch文件，按照下图红色框中的操作取消use_sim_time的注释（文件目录为：~/yugong_ws/src/cartographer_ros/cartographer_ros/launch/mapping_by_velodyne.launch）。

.. figure:: _static/lidar_mapping/use_sim_time.png
   :width: 80%
   :align: center
   :figclass: align-

3. 打开mapping_with_velodyne.launch文件，按照下图红色框中的操作将topic名字重命名(录制bag时的主机前缀与建图时的机器人主机名不一致，所以请手动remap激光建图软件的topic名称，与bag的topic名字保持一致，若不确定所录bag的topic名称，请参考下文步骤G的1与2进行查看，文件目录为：~/yugong_ws/src/cartographer_ros/cartographer_ros/launch/mapping_with_velodyne.launch)

.. figure:: _static/lidar_mapping/remap_topic.png
   :width: 80%
   :align: center
   :figclass: align-


4. 打开一个终端，输入以下命令打开激光建图软件。(ygxxxxxxx代表主机名)

::

	$ rosnode kill /ygxxxxxxx/cartographer_node
::

	$ source ~/yugong_ws/devel/setup.bash --extend
::

	$ roslaunch cartographer_ros mapping_by_velodyne.launch


G. 打开录制好的包并检查相关topic是否正常发布数据
++++++++++++

1. 使用以下命令打开录制的包

::

	$ rosbag play bag_name.bag --clock

2. 打开topic列表

::

	$ rostopic list

3. 使用以下命令检查相关topic是否正常发布数据（本文以velodyne、Imu及odom里程计为例子）

::

	$ rostopic echo /ygxxxxxxx/velodyne_points /imu /ygxxxxxxx/odom/wheel


::

	$ ^C (Ctrl+c)

::

	$ rostopic echo /imu

::

	$ ^C (Ctrl+c)

::

	$ rostopic echo  /ygxxxxxxx/odom/wheel

::

	$ ^C (Ctrl+c)

H. 设置rviz
++++++++++++

本步骤与在线激光建图的步骤C.设置rviz一致，请参考上文。

I. 建立地图并保存
++++++++++++

本步骤与在线激光建图的步骤D.建立地图并保存一致，请参考上文。

J. 配置激光定位软件参数并手动打开软件
++++++++++++

1. 建图步骤完毕后，请重启电脑使定位软件自动开启在该电脑上或运行以下命令手动打开定位软件

::

	$ source ~/yugong_ws/devel/setup.bash --extend
::

	$ roslaunch cartographer_ros localization_by_velodyne.launch

K. 检查激光雷达等驱动设备是否正常工作
++++++++++++

1. 该步骤与建图时检查激光雷达等驱动设备是否正常工作的步骤一致，请参考上文

L. 查看定位效果
++++++++++++

1. 打开rviz查看建立的图片，并运行机器人看点云扫描和所建立的图片是否吻合(彩色为已建的图，白色的点为定位的点云扫描，显示参数等调制请参考上文)。

.. figure:: _static/lidar_mapping/rviz.png
   :width: 80%
   :align: center
   :figclass: align-


Points_Cloud map(English)
------------------------

Online mapping with 4 steps

A. Configure parameters and run mapping function of Bitographer
B. Check the sensing device if works or not, such as Lidar
C. Setting for RVIZ
D. Build the map and save

Offline mapping with 5 steps

E. Record the bag(Suggest: record the bag while online mapping)
F. Configure parameters and run mapping function of Bitographer
G. Play the bag and check if the data was published successful from topic or not
H. Setting for RVIZ
I. Build the map and save

Localization by manual mode with 3 steps(Default boot)

J. Configure parameters and run localization function of Bitographer
K. Check the sensing device if works or not, such as Lidar
L. Check the accuracy of localization

A. Configure parameters and run mapping function of Bitographer
+++++++++++++++++++++++++++++++++++++++++++++++++++++++

1. A computer with Bitographer
  This situation will use Velodyne lidar and Imu to do mapping and localization
2. Configure parameters in mapping_with_velodyne.lua
  * Set 'use_imu_data=true'
  * File is located in the source code at src:~/yugong_ws/src/cartographer_ros/cartographer_ros/configuration_files/mapping_with_velodyne.lua
  * Without the source code, the file is located at src:~/yugong_ws/install/share/cartographer_ros/configuration_files/localization_with_velodyne.lua
  * Note, Bitographer need not be compiled after changing the parameters in lua files.

.. figure:: _static/lidar_mapping/mapping_with_velodyne_lua.png
   :width: 80%
   :align: center
   :figclass: align-

(Comment: Please check the pdf file to find more details about configuring parameters for different sensing devices :download:`configuring parameters for lua files <_static/lidar_mapping/lidar_mapping_configuration.pdf>`. Note that if wheel odometry sensor input is not being used, set **TRAJECTORY_BUILDER_3D.kalman_local_trajectory_builder.pose_tracker.use_odom_angle** to **false**)

3. Configure the urdf in mapping_with_velodyne.urdf to reflect the location of your sensors. The file can be found in src:~/yugong_ws/src/cartographer_ros/cartographer_ros/configuration_files/mapping_with_velodyne.urdf. For example, if the velodyne is centrally located 1 meter above the mobile base and has its local x and y axes aligned with the robot's x and y axes, change the origin of the velodyne joint to be <origin xyz="0.0 0.0 1.0" rpy="0 0 0" /> as shown below. Note that the box outlined in red defines the transform between **base_footprint** and **horizontal_vlp16_link** wherease the box outlined in blue defines the transform between **base_footprint** and **/velodyne**. Modify whichever one that corresponds to the frame the velodyne driver is publishing to. This can be determined by checking the launch file in the **velodyne_pointcloud** package. For example, in **VLP16_points.launch**, the data is published to the **horizontal_vlp16_link** frame and in **32e_points.launch**, the data is published to the **/velodyne** frame.

.. figure:: _static/lidar_mapping/urdf_red.png
   :width: 80%
   :align: center
   :figclass: align-

Note, Bitographer does not need to be recompiled after modifying the urdf.

4. If using the research platform (which uses a HDL32e LiDAR running the **32e_points.launch** file), open mapping_with_velodyne.launch, follow the picture with red frame to remap the velodyne topic name. File path:~/yugong_ws/src/cartographer_ros/cartographer_ros/launch/mapping_with_velodyne.launch). Note, Bitographer does not need to be recompiled after modifying the launch file.

.. figure:: _static/lidar_mapping/remap_velodyne.png
   :width: 80%
   :align: center
   :figclass: align-

4. Open another terminal and type the commands(ygxxxxxxx is hostname)

::

	$ rosnode kill /ygxxxxxxx/cartographer_node
::

	$ source ~/yugong_ws/devel/setup.bash --extend
::

	$ roslaunch cartographer_ros mapping_by_velodyne.launch



B. Check the sensing device if works or not, such as Lidar
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

1. Type the command to check the topic list,differrent devices with different topic name

::

	$ rostopic list


2. Use the commands to check the the sensing device if works or not (Comment: Please make sure of the topic name(hostname may not be in front) and what kind of sensing devices will be used first,and check each sensing device is working or not,such as imu)

::

	$ rostopic echo /ygxxxxxxx/velodyne_points

::

	$  ^C (Ctrl+c)

::

	$ rostopic echo /ygxxxxxxx/imu

3. If sensing device are not working, please use these commands (Different devices with different driver package). **Note, if using HDL32e LiDAR, replace VLP16_points.launch with 32e_points.launch**.

::

	$ roslaunch velodyne_pointcloud VLP16_points.launch (Velodyne' driver package)

::

	$ roslaunch xsens_driver xsens_driver.launch (Imu' driver package)

(Comment: Some sensing devices with USB interface such as imu need to use command to give the permission for USB interface)

::

	$  sudo chmod 777 /dev/ttyUSB*


C. Setting for RVIZ
+++++++++++++++++++

1. RVIZ will be opened automatic when do step A, but if RVIZ was not opened automatic, please use the command

::

	$ rosrun rviz rviz

2. Click "Add" and then "By topic" to add topics in RVIZ. Good topics for visualization include:
  * /map_cloud to see the 3D map being built as a pointcloud
  * /scan_matched_points2 to see where the current scan matches the points in the map
  * /velodyne_points to see the velodyne pointcloud output
  * /cam_estimated_pose1 to see where the robot the robot pose is within the map

  To visualize the robot model, add it through the "Display Type" tab. Then modify the RobotModel's Robot Description and TF Prefix arguments in RVIZ with /<hostname>/yugong_description and /<hostname> respectively.

.. figure:: _static/lidar_mapping/add.png
   :width: 80%
   :align: center
   :figclass: align-


.. figure:: _static/lidar_mapping/topic_choose.png
   :width: 80%
   :align: center
   :figclass: align-

(Comment: If the robot used does not have a dispaly, please comment out the rviz node in mapping_by_velodyne.launch. Otherwise, it will cause Bitographer to not work. Follow the picture with red frame. File path:~/yugong_ws/src/cartographer_ros/cartographer_ros/launch/mapping_by_velodyne.launch)

.. figure:: _static/lidar_mapping/rviz_close.png
   :width: 80%
   :align: center
   :figclass: align-


D. Build the map and save
+++++++++++++++++++++++++

1. Move the robot around the factory (Try to navigate the robot through the entire site - the starting point and the ending point need to be the same. Record bags for offline mapping while mapping online, more details about recording bag and offline mapping please find in next steps)

2. Open another terminal and type the commands

::

	$ source ~/yugong_ws/devel/setup.bash --extend

::

	$ rosservice call "/ygxxxxxxx/finish_trajectory" "map"

::

	$ cd ~/.ros

::

	$ cp map.pcd ~/catkin_slam/pcd_map/map.pcd

(Comments: If there is no catkin_slam directory, please create the catkin_slam folder with a pcd_map subfolder in the root directory or store the map.pcd file in the ~/.ros directory in a custom location. Just make sure the file path matches the file path set to the map_pcd_name parameter located within the localization_with_velodyne.lua file. Otherwise, the map cannot be successfully located when using the localization software. The configuration file path of the source code is: src:~/yugong_ws/src/cartographer_ros/cartographer_ros/configuration_files/localization_with_velodyne.lua. The file path without source code is src:~/yugong_ws/install/share/cartographer_ros/configuration_files/localization_with_velodyne.lua)

.. figure:: _static/lidar_mapping/lua.png
   :width: 80%
   :align: center
   :figclass: align-

3. Close the Bitographer when complete and save the map.

E. Record the bag(Comment: can record bag while online mapping)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

1. Type the command to check the topic list

::

	$ rostopic list

2. Use the command to record the bag. (Suggest: record all sensing devices' topic (hostname may not be in front), in this situation will record velodyne, imu and odom topics)

::

	$ rosbag record /ygxxxxxxx/velodyne_points /ygxxxxxxx/imu /ygxxxxxxx/odom/wheel

3. Move the robot around the factory (Try to navigate the robot through the entire site - the starting point and the ending point need to be the same. Use the command to stop recording.

::

	$ ^C (Ctrl+c)

F. Configure parameters and run mapping function of Bitographer
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

1. Open mapping_with_velodyne.lua to configure parameters, make sure what kinds of sensing device'topic are recorded and changing the parameters like online mapping.

2. Open mapping_by_velodyne.launch,follow the picture with red frame to cancel the comment for 'use_sim_time'.(File path:~/yugong_ws/src/cartographer_ros/cartographer_ros/launch/mapping_by_velodyne.launch)

.. figure:: _static/lidar_mapping/use_sim_time.png
   :width: 80%
   :align: center
   :figclass: align-

3. Open mapping_with_velodyne.launch, follow the picture with red frame to remap the topic name (Different computer to record the bag with different hostname, please remap Bitographer' topic name to make sure the topic name are the same with bag's topic name,if you are not sure the bag's topic name, please follow step G 1 and 2. If the bag file is being run on a different computer, then launch a static transform publisher between the frames associated with the different computer's hostname (such as for velodyne and IMU) to the frame of the robot as shown below. File path:~/yugong_ws/src/cartographer_ros/cartographer_ros/launch/mapping_with_velodyne.launch)

  Static TF example for velodyne:
  ::

    <node pkg="tf" type="static_transform_publisher" name="bito_to_robot" args="0 0 0 0 0 0 bitol15/horizontal_vlp16_link yg00a00019040513000g00/horizontal_vlp16_link 100" />

  or

  ::

    rosrun tf static_transform_publisher 0 0 0 0 0 0 bitol15/horizontal_vlp16_link yg00a00019040513000g00/horizontal_vlp16_link 100

.. figure:: _static/lidar_mapping/remap_topic.png
   :width: 80%
   :align: center
   :figclass: align-

4. Open a terminal and type the command to run the software(ygxxxxxxx is hostname)

::

	$ rosnode kill /ygxxxxxxx/cartographer_node
::

	$ source ~/yugong_ws/devel/setup.bash --extend
::

	$ roslaunch cartographer_ros mapping_by_velodyne.launch


G. Play the bag and check if the data was published successful from topic or not
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

1. Use the command to play the bag

::

	$ rosbag play bag_name.bag --clock

2. Check topic list

::

	$ rostopic list

3. Type the command in terminal to check if the data was published successful from topic or not (In this situation will check velodyne, Imu and odom'topic. Note, hostname may not be in front)

::

	$ rostopic echo /ygxxxxxxx/velodyne_points /ygxxxxxxx/imu /ygxxxxxxx/odom/wheel


::

	$ ^C (Ctrl+c)

::

	$ rostopic echo /ygxxxxxxx/imu

::

	$ ^C (Ctrl+c)

::

	$ rostopic echo  /ygxxxxxxx/odom/wheel

::

	$ ^C (Ctrl+c)


H. Setting for RVIZ
+++++++++++++++++++

This step is the same with online mapping' step C


I. Build the map and save
+++++++++++++++++++++++++

This step is the same with online mapping' step D

J. Configure parameters and run localization function of Bitographer
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

1. Configure **localization_with_velodyne.lua** as was done in section A step 2 by mapping.
2. Configure **localization_with_velodyne.urdf** as was done in section A step 3 by mapping.
3. Configure **localization_with_velodyne.launch** as was done in section A step 4 by  mapping.
4. Run the software of manual mode by using the commands.

::

	$ source ~/yugong_ws/devel/setup.bash --extend
::

	$ roslaunch cartographer_ros localization_by_velodyne.launch

K. Check the sensing device if works or not, such as Lidar
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

1. This step is the same with online mapping' step B

L. Check the accuracy of localization
+++++++++++++++++++++++++++++++++++++

1. Open RVIZ to check the map_cloud and points_cloud is matching or not(Colorful points are the map_cloud, white points are points_cloud, setting RVIZ is the same with online map)

.. figure:: _static/lidar_mapping/rviz.png
   :width: 80%
   :align: center
   :figclass: align-

2. If localization is taking a long time to occur (or is not occuring), try bringing the robot close to the map origin and/or modify the parameter below in **localization_with_velodyne.lua** to be 0.0. Then relaunch localization.

  TRAJECTORY_BUILDER_3D.kalman_local_trajectory_builder.initial_pose_z = 0.0
