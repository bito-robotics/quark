HTTP API v1.0
=====================

.. include:: hanxin_task_api.rst
.. include:: hanxin_robot_api.rst
.. include:: hanxin_monitor_api.rst
.. include:: hanxin_tag_map_api.rst
.. include:: hanxin_roadmap_api.rst