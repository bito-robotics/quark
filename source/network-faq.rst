网络异常
-------------------

1、如需传感器联通上层网络至Hanxin

配置NAT

在yugong主机上依次执行

::

    $ ip_forward :  echo 1 > /proc/sys/net/ipv4/ip_forward  
    $ iptables -F
    $ iptables -P INPUT ACCEPT
    $ iptables -P FORWARD ACCEPT
    $ iptables -t nat -A POSTROUTING -o wlp58s0 -j MASQUERADE     //wlp58s0为yugong主机接外网的网卡


2、有线端口重启错误解决

重启接口的过程中可能会出现错误

::

    Error: /etc/resolv.conf isn't a symlink

解决方法：
把 ``/etc/resolv.conf`` 文件拷贝到目录： ``/run/resolvconf/`` 下

::

    $ sudo mv /etc/resolv.conf /run/resolvconf/

建立软连接：

::

    $ sudo ln -s ../run/resolvconf/resolv.conf  /etc/resolv.conf

