设计路网
==============

简介
------------

本文档旨在提供一个BITO路网软件系统操作指南，使经培训后的BITO系统操作人员能够完成路网的设计及操作。

基本概念
------------

1.路网，路网是工厂中的交通网络，是机器人允许行走的所有可能的路线的集合。数学上，BITO使用的路网是一个有向图。

2.路网节点（简称节点），是交通网络中的一个个点位。数学上，是有向图的节点。

3.路网边（简称边），是交通路网中连接两个点位之间的有向线段，从起点节点指向终点节点。数学上，是有向图中的有向边。

4.逻辑库位，是抽象的位置名称，用于对接BITO外部的系统。可用于表示外部系统仓储库位，关键点位，等等。

5.逻辑库位映射关系，或称映射表，是一个把逻辑库位一一对应到路网节点的映射。一个逻辑库位对应唯一的路网节点，反过来，一个路网节点可以被多个逻辑库位对应。


路网设计步骤
------------
	
1.在火狐浏览器中输入Hanxin的IP地址：192.168.XXX.XXX：5000（每台Hanxin的IP地址不同，请自行查询，如图１所示）。

.. figure:: _static/roadmap/Selection_001.png
   :width: 80%
   :align: center
   :figclass: align-

   图1 火狐填入IP

2.进入Hanxin的UI界面后，点击“DESIGN”开始进行路网的设计（点击“DESIGN”的时候请同时按键盘上的“Ctrl”以开启新标签窗口，如若不按“Ctrl”则Hanxin的主页面会被自动覆盖）。

3.请在左边菜单栏“Select An Existing TagMap”　的下拉菜单中选择一个合适的＂Tag Map＂ 后点击对应的”Select”并自行选择一个合适的Tag Map的尺寸（如图２所示）。

.. figure:: _static/roadmap/tagmap.png
   :width: 80%
   :align: center
   :figclass: align-

   图2 Tag Map的操作栏

4.接着在“Select An Existing RoadMap”下拉菜单中按择一张想用作基底的路网图。选定后请按对应的“Select”，立马会弹出一个对话框问是否确认选定该路网，选择OK后点击“Load”加载路网图（如图３所示）。

.. figure:: _static/roadmap/roadmap.png
   :width: 80%
   :align: center
   :figclass: align-

   图3 RoadMap的操作栏

5.当用于基底的路网图被加载出来后请按“Save your Roadmap”下的“Save As New”保存成一个新的录完图（该步骤是进行备份，在备份中进行路网的修改，以防作为基底的路网图是他人所需要使用的,如图3所示）。

6.请刷新整个Hanxin页面。

7.重复步骤４打开自己新建的路网。

8.选择左边的Delete删除不需要的节点以及连接线。删完后请按”Esc”进行该步骤的退出（如果想要一个空白画布，请在“Save your Roadmap”下点击”Empty”进行清空，如图３所示）。

9.删除不需要的逻辑点，点击“Delete Logic”的”Delete Selected”后点击图片中不需要的逻辑点（节点上的三角形）进行删除。

.. figure:: _static/roadmap/logic.png
   :width: 80%
   :align: center
   :figclass: align-

   图4 Logic的操作栏

10.添加节点，请点击“Add”后在画布任意一处点击一次创建节点，接着在节点所需对应的方向上任意空白再处点击一次已设定节点方向。创建完所有需要的节点后，请点击Esc进行退出（如图3所示）。

11.调整节点位置，点击“Edit”后点击需要修改的节点的位置。将节点的X,Y,角度等参数依次填入并按“Confirm”（如图３所示）。

   节点位置注意事项： 详见注意事项

12.连接节点，请按“Connect”后先点击作为起始的节点再点击作为目标的节点（点击的时候请注意看Help栏的Connect Status已判断是否成功选中节点，如图３及图５所示）。

.. figure:: _static/roadmap/connect.png
   :width: 80%
   :align: center
   :figclass: align-

   图5 Help栏

13.添加逻辑点，请右键点击后在Edit Selected Logic Name中输入逻辑节点名或右键点击节点进行操作，接着按“Add Logic”（如图４及图６所示）。

.. figure:: _static/roadmap/logic_name.png
   :width: 80%
   :align: center
   :figclass: align-

   图6 右键点击节点添加逻辑

14.”Save”后请查看自己设计的路网是否成功保存（请重复步骤４）。　

15.返回到“MONITOR”已查看设计的路网以及机器人在路网上的状态（如图７所示）。

.. figure:: _static/roadmap/monitor.png
   :width: 80%
   :align: center
   :figclass: align-

   图7 MONITOR界面


等待区（waiting area）添加步骤
------------

1.在设计路网时，若需要添加等待区（机器人空闲后，会自动进入等待区待命），则需要在不影响主路网的区域设置等待区（如下图所示的WA2:矩形区域和WA4:三角形区域），逻辑点命名以"WA"开头。

.. figure:: _static/roadmap/WA_setting.png
   :width: 80%
   :align: center
   :figclass: align-

   图8 在＂design＂页面中添加等待区

2.在hanxin_ws/src/bwms/bwms/script/python/文件夹下，运行


::

    $ python create_bwms_database.py

３.在hanxin_ws/src/bwms/bwms/script/python/insert_sample_bwms_database_JS.py文件下, 将开头为"WA"的逻辑点加入脚本，然后运行脚本文件。


::

    $ python insert_sample_bwms_database_JS.py

.. figure:: _static/roadmap/WA_setting_2.png
   :width: 80%
   :align: center
   :figclass: align-

   图9 在脚本中添加等待区信息

4.在数据库中检查等待区是否被加入bwms_database。

.. figure:: _static/roadmap/WA_setting_3.png
   :width: 80%
   :align: center
   :figclass: align-

   图10 在数据库中查看等待区信息

5.手动启动bwms(有的hanxin是自启动，有的需要自行手启)


::

    $ roslaunch bwms_ros node_core_task_scheduler_greedy_2d_js.launch

开启一个新的终端

::

    $ rosservice call /switch_bwms_server_status (然后按tab键补全, 将command改为1)


注意事项
------------ 
    
1.按着鼠标中间的滚轮移动位置。

2.设计路网时请及时按“Esc”退出并保存当前操作。

3.点击“Edit”后点击节点或移动到节点所在位置可以知道当前节点的位置参数。

4.每次选择路网前都应进行刷新操作。 

5.在添加节点位置时，应根据该路网使用的机器人进行设计。

在使用类kiva的机器人的时候，该路网将不会有所限制。而对于使用曲线轨迹的路网来说（这部分路网可能会跑叉车等差动轮机器人），需要注意下一个节点与上一个节点间的角度和距离。下面是对曲线路网设计的一些关键点。

1）原则上，应该尽可能保证在直线路网上，上下两个节点以及上节点到下节点的的方向一致。

2）如果需要转弯，建议保证叉车的路网，上节点和下节点有2m以上的转弯半径，对于小型的agv的路网，有1m以上的转弯半径。转弯部分两个节点设计应该尽可能对称，角度建议相差90度，以满足路网的美观简洁，以及实现轨迹流畅的需求。

如图11，直线上的节点的方向和路网连线方向一致，同时转弯时，有1m的转弯半径且两个节点方向相差90度，且每个转弯上的两个节点对称分布在曲线两侧，最终显示效果如图12 。


.. figure:: _static/roadmap/roadmap_design_classic1.png
   :width: 80%
   :align: center
   :figclass: align-

   图11 曲线路网设计案例

.. figure:: _static/roadmap/roadmap_design_classic2.png
   :width: 80%
   :align: center
   :figclass: align-

   图12 曲线路网显示案例


