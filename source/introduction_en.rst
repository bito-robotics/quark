Introduction
============

This manual is intended to help users successfully install, use, and develop
code on BITO's Quark system. The software installed on
both robots is based on ROS. Please visit ros.org to learn more about ROS.

All users should read and become familiar with the safe operating procedures
set out in :doc:`Safety Section</safety>` before operating a robot.

Before you Start
----------------

Before getting started, below is an overview of what you need to use and operate
Quark safely.

* **BITO Safety** 
 
 - Read :doc:`Safety Section</safety>` in its entirety before using either robot.

* **Safe Environment** 
 
 - Only operate Quark in an environment free of hazards. Specifically,
   stairways and large drop offs can cause personal injury and extreme damage.
   Avoid dangerous objects, sharp objects (such as knives), fire sources, or
   hazardous chemicals. A robot with a knife or hot object is inherently
   dangerous.

* **Space** 

 - Make sure that there is enough space for Yugong to drive around
   and perform tasks. Yugongs are designed to operate in ADA-compliant
   environments (Americans with Disabilities Act).

* **Development Tools** 
 
 - To connect with Yugong, a laptop or desktop computer is needed.
   Ideally Hanxin and Yugong can connect to a wireless network but users can
   also directly to Hanxin via the Ethernet port. 

* **Linux**
 
 - Familiarity with the Linux command-line is strongly recommended. The computers
   in Yugong have Ubuntu Linux installed. Tasks can be performed
   by logging in remotely by using ssh or other similar tool. Users can also
   directly plug in a display, mouse, and keyboard to the access panels on
   Quark.

* **ROS**

 - Quark R&D software is based on ROS. Completing the
   beginner tutorials will help users understand how to operate and
   write applications for the Quark.

* **BITO Support**

 - Please visit BITO Robotics support portal at http://support.bitorobotics.com
   and login to review service information, modularity specifications, important
   safety updates, and submit hardware or software support tickets for your robot.
