自动充电系统软硬件部署
======================

概述
----

BITO移动机器人自动充电系统包含充电控制系统和充电调度系统。

充电控制系统作为嵌入式实时充电控制系统，其主要对底层充电设备提供强实时性管理控制，并为上层业务提供稳定可靠的通信服务，由充电桩系统和充电桩控制系统构成。自动充电调度系统作为上层应用，其依赖于充电控制系统来完成充电调度业务，其包含数据通信服务、数据存储服务、充电管理服务。详细设计与说明请参见其他自动充电相关文档，以下仅对充电控制和调度系统部署作简单描述。

充电控制系统部署
----------------

充电桩配置指南
~~~~~~~~~~~~~~

1. 电脑连接串口线到充电桩控制器
2. 查询串口号: 终端使用指令\ ``ls -l /dev/ttyUSB*``, 将读取的串口号记下,
   控制器板有两个串口号, 这个\ ``需要在cutecom中尝试``
3. 打开cutecom, 如下配置:
   其中\ ``ttyusb0需要更改为步骤2中看到的串口号, 尝试其中的一个串口之后点击open device即可, 如果没有数据, 请尝试另一个串口号``

.. figure:: ./_static/charging_controller_config.png
   :alt: charging\_controller\_config

   charging\_controller\_config

4. 配置HX IP地址:
   先设置cutecom右下角为\ ``CR,LF line end``\ 在下端的\ ``Input``\ 处输入\ ``hx ip 192.168.103.189:63800``\ 回车即可,
   然后再输入\ ``reboot``\ 回车即可

充电调度系统部署
----------------

自动充电服务器为自动充电调度系统提供数据通信服务，包含多会话管理、可靠稳定的数据通信、充电业务包封装解析、数据管理分发、定时器管理、充电业务控制及调度等功能。通过此服务，上层可与底层充电控制系统完成业务交互。

自动充电服务在BITO机器人系统中作为后台服务器运行于韩信系统内部，其对外部通信主要采用socket机制与其他模块或外设交互，是建立在TCP/IP框架上的通信，因此依赖于有效的网络环境。
本节主要描述自动充电服务的配置部署。

譬如要在一个封闭的工厂内构建一个局部的自动充电系统，那么,我们需要完成以下几个步骤:

(以下以武汉深海弈智软件包为例)

构建厂区局域网
~~~~~~~~~~~~~~

构建厂区局域网，BITO机器人自动充电系统将要部署在该局域网内，如此充电控制系统和充电调度系统便能通过局域以太网通信。例如，我们建立的局域网只使用C类ipv4
192.168.1.0/24子网段。

部署设备资源
~~~~~~~~~~~~

确保充电控制系统硬件资源都已部署，其主要包括充电桩和充电桩控制器；确保充电调度系统硬件资源都已部署，其主要包括韩信设备和AGV小车。同时确认所有硬件资源能上电正常工作，且都与厂内局域网建立通信。

部署自动充电服务
~~~~~~~~~~~~~~~~

自动充电服务器部署于韩信操作系统平台，请确认软件包版本系统与韩信系统的兼容性。

-  Step1: 下载自动充电服务软件包

`BITO外网FTP下载地址 <ftp://180.167.101.46/release/auto_charger/deepsea/V1.0.0/charger_server/deepsea-auto-charger-server.tar.xz>`__

`BITO内网FTP下载地址 <ftp://192.168.1.181/release/auto_charger/deepsea/V1.0.0/charger_server/deepsea-auto-charger-server.tar.xz>`__

下载用户名为\ ``bito``\ ，密码为\ ``Bito@2050``

Ubuntu系统界面，操作示例如下:

.. code:: shell

    $ cd ~ && wget ftp://180.167.101.46/release/auto_charger/deepsea/V1.0.0/charger_server/deepsea-auto-charger-server.tar.xz –ftp-user=bito –ftp-password=Bito@2050

-  Step2: 配置自动充电服务 解压软件包，并安装。

Ubuntu系统界面，操作示例如下:

.. code:: shell

    $ cd ~ && tar xf deepsea-auto-charger-server.tar.xz && sudo cp deepsea-auto-charger-server/* / -rf 
    $ cat /etc/mmcp/config.ini 

**输出的内容**

.. code:: shell

    # initialization configure of automatic charger 
    # 指定存放日志的路径 
    log_path=~/hanxin_ws/src/hanxin/hanxin/log/ 
    # 设置当前设备韩信的网卡局域以太网ipv4地址，该网卡对外与充电桩控制器连接通信。 若韩信有多个网卡，请事先确认找到能与充电桩控制器连接的网卡ip，并设置。自动充电服务提供了ip自动查找匹配功能，一般情况下，该参数可不设置，保留空值
    server_ip= 
    # 设置自动充电服务的端口号，该值要与充电控制器所配置端口号保持一致。一般情况下，该参数可不设置，默认与充电控制器保持一致，且值为63800。
    service_port=63800 

-  Step3: 启动自动充电服务 服务作为守护进程运行于韩信后台。

Ubuntu系统界面，操作示例如下:

.. code:: shell

    $ /usr/local/bin/mmcp_se_V1.0.0_x86

-  Step4: 确认服务

确认服务已正常启动运行。

Ubuntu系统界面，操作示例如下:

.. code:: shell

    # 查看后台进程 
    $ ps aux | grep -i mmcp_se_V1.0.0_x86 
    # 或查看日志文件输出
    $ sudo tail -f -n50 ~/hanxin_ws/src/hanxin/hanxin/log/deepsea-auto-charger-server/deepsea-auto-charger-server##date##.log 

部署ROS项目
~~~~~~~~~~~

下载BITO ROS charger源码，编译安装。

-  Step1: charger repo下载编译

.. code:: shell

     $ cd ~/hanxin_ws/src && git clone */charger.git && cd ../ && catkin_make -j4

-  Step2: 修改yaml配置文件

.. code:: shell

     $ cat ~/hanxin_ws/src/charger/param/embedded_charging.yaml

**输出的内容:**

.. code:: shell

    # 设置当前设备韩信的网卡局域以太网ipv4地址，用于与自动充电服务通信，同上自动充电服务配置中server_ip配置。
    # charger已提供了ip自动查找匹配功能，一般情况下，该参数可不设置，保留空值。
    /embedded_charging/server_string_ip4: “” 
    # 设置charger的端口号，用于与自动充电服务通信。
    # 一般情况下，该参数可不设置，默认与充电控制器保持一致，且值为63800。
    /embedded_charging/charging_service_port: 63800 

-  Step3: 启动charger进程

.. code:: shell

    $ cd ~/hanxin_ws && source devel/setup.bash 
    # 该进程依赖与自动充电服务mmcp_se_V1.0.0_x86, 若进程在启动该服务后再启动，若服务down掉，则该进程则无法正常工作。 
    $ roslaunch charger_ros embedded_charging.launch 
    # 该进程为充电调度进程，与embedded_charging进程建立通信。
    $ roslaunch charger_ros node_charging_manager.launch 

自动充电应用
~~~~~~~~~~~~
