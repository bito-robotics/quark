Yugong API v1.0
===============

.. include:: yugong_monitor_api.rst
.. include:: yugong_controller_api.rst
.. include:: yugong_task_api.rst
