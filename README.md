BITO Robotics Research Edition Documentation
==============================================

This is the source for the BITO Robotics Research Edition Documentation,
which is  hosted at http://docs.bitorobotics.com

Building New Releases
---------------------

Install prerequisites:

```
sudo pip install sphinx
sudo pip install sphinx_rtd_theme
```

Build new release from master, store into gh-pages branch:

```
git checkout master
make html
make latexpdf
git checkout gh-pages
cp -r build/html/* .
cp build/latex/BitoRobotics.pdf .
rm -rf build
git commit -a -m "new build"
git push origin gh-pages
```

Troubleshooting:

If you get complaints about .sty files install the following

```
sudo apt-get install texlive-full
```
